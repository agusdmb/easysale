// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic-material','starter.controllers','starter.directives'])
.run(function($ionicPlatform,$rootScope,orderService) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
  $rootScope.$on('$stateChangeStart',function(event,to,toParams,from,fromParams){
    if(from.name == 'app.new_order' && (to.name == 'app.pedidos' || to.name == 'app.client')){
      orderService.reset();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: "AppCtrl"
  })
  .state('app.clients', {
    url: "/clients",
    views: {
      'menuContent': {
        templateUrl: "templates/clients.html",
        controller: "ClientsCtrl"
      }
    }
  })
  .state('app.pedidos', {
    url: "/pedidos",
    views: {
      'menuContent': {
        templateUrl: "templates/pedidos.html",
        controller: 'PedidosCtrl'
      }
    }
  })
  .state('app.pedido', {
    url: "/pedido/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/pedido.html",
        controller: 'PedidoCtrl'
      }
    }
  })
  .state('app.new_order', {
    url: "/new_order",
    views: {
      'menuContent': {
        templateUrl: "templates/new_order.html",
        controller: 'NewOrderCtrl'
      }
    }
  })
  .state('app.client', {
    url: "/client/:id",
    views: {
      'menuContent':{
        templateUrl: "templates/client.html",
        controller: 'ClientCtrl'
      }  
    }
  })
  .state('app.catalog', {
    url: "/catalog",
    views: {
      'menuContent': {
        templateUrl: "templates/catalog.html",
      }
    }
  })
  .state('app.product_selection', {
    url: "/product_selection",
    views: {
      'menuContent': {
        templateUrl: "templates/product_selection.html",
      }
    }
  })
  .state('app.category', {
    url: "/category/:id",
    views: {
      'menuContent': {
        templateUrl: "templates/category.html",
        controller: 'CategoryCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/clients');
});