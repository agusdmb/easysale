angular.module("starter.services",['data'])
    .service('orderService',function(pedidos,productos){
        this.on_service = false;
        this.actual_order = {'id':pedidos.length,
                            'id_client':null,
                            'date' : null,
                            'ordered_products':[]};
        this.startService = function(){
            this.on_service = true;
        }
        this.stopService = function(){
            this.on_service = false;
        }
        this.add_product = function(id){
            console.log(id);
            for(i=0;i<productos.length;i++){
                if(productos[i].id == id){
                    ordered_product = {'id':id,'cant':null,'uprice':productos[i].price};
                    this.actual_order.ordered_products.push(ordered_product);
                };
            };
        };
        this.select_product = function(id){
            if(this.on_service){
                this.add_product(id);
                return true;
            }else{
                return false;
            }
        }
        this.get_order_products = function(){
            return this.actual_order.ordered_products;
        }
        this.get_client_id = function(){
            return this.actual_order.id_client;    
        }
        this.set_client_id = function(id){
            this.actual_order.id_client = id;    
        }
        this.get_date = function(){
            return this.actual_order.date;
        }
        this.set_date = function(date){
            this.actual_order.date = date;
        }
        this.reset = function(){
            this.actual_order = {'id':pedidos.length,
                                'id_client':null,
                                'ordered_products':[]};
        }
        this.save_order = function(){
            pedidos.push(this.actual_order);
        }
        this.start_order_for_client = function(id){
            this.set_client_id(id);
            today = new Date();
            var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            today_date = today.getDate() + " de " + meses[today.getMonth()] 
            this.set_date(today_date);
        }
    });