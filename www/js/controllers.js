angular.module('starter.controllers', ['data'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PedidosCtrl', function($scope, $state, $ionicModal, orderService, pedidos, productos, clients){
    
  $ionicModal.fromTemplateUrl('templates/client_selector.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });
  
  this.pedidos = pedidos;
  
  this.hizo_click = function(id){
      $state.go('app.pedido', {'id':id});
  }
  
  this.productos = productos;

  this.get_cliente = function(id){
      for(i = 0; i < clients.length; i++){
          if(clients[i].id == id){
              return clients[i].name;
          }
      }
      return "sin nombre";
  }
  
  this.get_cliente_img = function(id){
      for(i = 0; i < clients.length; i++){
          if(clients[i].id == id){
              return clients[i].avatar;
          }
      }
      return "sin nombre";
  }
  
  $scope.$parent.sel_client_open = function(){
    $scope.modal.show();
  }

  $scope.$parent.sel_client_close = function(){
    $scope.modal.hide();
  }
  $scope.$parent.select_client = function(id){
    orderService.start_order_for_client(id);
    $state.go('app.new_order');
    $scope.modal.hide();
  }
})

.controller('PedidoCtrl', function($stateParams, pedidos, productos, clients){
    console.log($stateParams);
    for(var i = 0; i < pedidos.length; i++){
        if (pedidos[i].id == $stateParams.id){
            this.pedido = pedidos[i];
            this.productos = this.pedido.ordered_products;
            console.log(this.productos);
        }
    }
    this.get_cliente = function(){
        var id = this.pedido.id_client;
        for(var i = 0; i < clients.length; i++){
            if(clients[i].id == id){
                return clients[i].name + " " + clients[i].lastname;
            }
        }
        return "sin nombre";
    }
    this.get_producto = function(id){
        for (var i = 0; i < productos.length; i++) {
          if (productos[i].id == id){
            return(productos[i]);
          }
        };
    }
    this.get_total = function(){
        var total = 0;
        for (var i = 0; i < this.productos.length; i++) {
            total = total + this.productos[i].cant * this.get_producto(this.productos[i].id).price;
        };
        return total;
    }
    this.get_fecha = function(){
        return this.pedido.date;
    }
})

.controller('NewOrderCtrl', function($scope,$state,clients,productos,orderService,$ionicHistory){
  var id = orderService.get_client_id();
  
  for(i=0;i<clients.length;i++){
    if(clients[i].id == id){
      $scope.client = clients[i];
    }
  }
  orderService.stopService(); // Nos aseguramos de detener el servicio de agregar producto  
  
  $scope.order_products = orderService.get_order_products();
  
  $scope.get_product = function(id){
    for (var i = 0; i < productos.length; i++) {
      if(productos[i].id == id){
        return productos[i]
      };
    };
  };
  $scope.delete_product = function(id){
    console.log("hello");
  }
  
  $scope.$parent.addProduct = function(){
    orderService.startService();
    $state.go('app.product_selection');
  };
  $scope.$parent.saveOrder = function(){
    orderService.save_order();
    $ionicHistory.goBack();
  }
  $scope.get_total = function(){
    var result = 0;
    for (var i = 0; i < $scope.order_products.length; i++) {
      result += $scope.order_products[i].uprice * $scope.order_products[i].cant;
    };
    return result;
  }
})

.controller("ClientsCtrl", function($state,$scope,$timeout,clients,$ionicScrollDelegate){
  $scope.clients = clients;
  $scope.$parent.currentletter = ''
  $scope.letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $scope.startwith = function(a,b){
    return a[0] == b;
  }
  $scope.handle = function(){
    var letters = document.querySelectorAll('.letter');
    var found = false;
    var i = 0;
    while(!found && (i!=(letters.length)-1)){
      var scroll_top = $ionicScrollDelegate.getScrollPosition().top;
      if (letters[i].offsetTop < scroll_top){
        i+=1;
      }else{
        found = true;
      }
    }
    var currentletter = letters[i-1];
    $scope.$parent.currentletter = angular.element(currentletter).text();
    var currentlettermsg = document.querySelector('.currentletter');
    angular.element(currentlettermsg).removeClass("ng-hide");
    console.log($scope.currentletter);
    $scope.$apply();
  }
  // $scope.hide_letter = function(){
  //   console.log("entro")
  //   var currentlettermsg = document.querySelector('.currentletter');
  //   angular.element(currentlettermsg).addClass("ng-hide");    
  // }
  // ionicMaterialMotion.fadeSlideInRight(); No funciona con ng-repeat todavia, bug de ionic-material
})


.controller("ClientCtrl",function($scope,$state,$stateParams,clients,pedidos,orderService){
  var id = $stateParams.id;
  for(i=0;i<clients.length;i++){
    if(clients[i].id == id){
      $scope.client = clients[i];
    }
  }
  $scope.getClientOrders = function(id){
    var mis_pedidos = [];
    for(i=0;i<pedidos.length;i++){
      if(pedidos[i].id_client == id){
         mis_pedidos.push(pedidos[i]);
      }
    }
    return mis_pedidos;
  }
  $scope.$parent.addOrder = function(id){
    orderService.start_order_for_client(id);
    $state.go('app.new_order');
  }
})

.controller('CatalogCtrl', function($scope,$filter,categories){
  $scope.categories = $filter('filter')(categories,{'parent_id':null});
})

.controller('CategoryCtrl', function($scope,$stateParams,$filter,categories,productos){
  var id = $stateParams.id;
  for(i=0;i<categories.length;i++){
    if(categories[i].id == id){
      $scope.category = categories[i];
    }
  }
  $scope.categories = $filter('filter')(categories,{'parent_id': parseInt(id)},true);

  $scope.products = $filter('filter')(productos,{'category_id': parseInt(id)},true);
});