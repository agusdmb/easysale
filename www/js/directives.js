angular.module('starter.directives',['starter.services'])
.directive('myCategories',function(){
    return {
        templateUrl : 'templates/categories.html',
        scope : {
            categories : '=categories'
        }
    }
})
.directive('myProducts',function(orderService){
    return {
        templateUrl : 'templates/products.html',
        scope : {
            products : '='
        },
        controller : function($scope, $ionicHistory, orderService){
            $scope.myonclick = function(id){
                var historial = $ionicHistory.viewHistory().histories.ion1.stack;
                for (var i = 0; i < historial.length; i++) {
                    if (historial[i].stateName == "app.new_order"){
                        var index = historial[i].index;
                        break;
                    }
                };
                if (orderService.select_product(id)){
                    $ionicHistory.goBack(-(historial.length-1 - index));
                }
            }
        }
    }   
})
.directive('initFocus', function() {
    var timer;

    return function(scope, elm, attr) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(function() {
            elm[0].focus();
            console.log('focus', elm);
        }, 0);
    };
});